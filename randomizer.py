import pygame
from pygame.locals import *

import random, time
import os, sys

if __name__ == '__main__':
    pygame.init()
    pygame.font.init()

    base_path=""
    img_path = "img"

    icon = pygame.image.load(os.path.join(img_path, "ico.jpg"))
    pygame.display.set_icon(icon)

    pygame.display.set_caption("UP ACM Algolypics 2015 - Randomizer")
    SCREEN = pygame.display.set_mode((600, 600))
    BG = pygame.Surface(SCREEN.get_size()).convert()
    BG.fill((0, 0, 0))

    font = pygame.font.Font(None, 300)
    images = []
    for i in xrange(1, 28):
        # images.append(font.render(str(i), True, (255, 255, 255)))
        images.append(pygame.image.load(os.path.join(img_path, str(i)+"-01.png")))
        images[i-1] = pygame.transform.scale(images[i-1], (600, 600))

    font2 = pygame.font.Font(None, 24)
    instruction = font2.render("Press Space to randomize", True, (255, 255, 255));
    instruction_rect = instruction.get_rect()
    instruction_rect.center = 300, 550

    randomizing = False
    randomizingTime = time.time()
    running = True
    image = font.render("-", True, (255, 255, 255))
    clock = pygame.time.Clock()
    while(running):

        clock.tick(60)
        for event in pygame.event.get():
            if event.type == QUIT:
                running = False
            elif event.type == KEYDOWN:
                if event.key == K_SPACE and len(images) > 0 and not randomizing:
                    randomizing = True
                    randomizingTime = time.time()

        if(randomizing):
            image = random.choice(images)
            if(time.time() - randomizingTime >= 1):
                randomizing = False
                images.remove(image)

        SCREEN.blit(BG, (0, 0))
        rect = image.get_rect()
        rect.center = 300, 300
        SCREEN.blit(image, rect)
        SCREEN.blit(instruction, instruction_rect)

        pygame.display.flip()
