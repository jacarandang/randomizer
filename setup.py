import sys
from os import path
from cx_Freeze import setup, Executable
import re
# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os", "sys", "pygame", "random", "time"], "excludes": ["tkinter"], "include_files": ["img"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "UP ACM Randomizer",
        version = "0.1",
        description = "UP ACM Randomizer for Algolympics",
        options = {"build_exe": build_exe_options},
        executables = [Executable("randomizer.py", base=base)])
